'use strict';

var n = '';
var k = '';
var k2 = k;
var newNumberSmallest = '';

function init(){

	var num = document.getElementById('num').value;
	var digit = document.getElementById('k-digit').value;

	n = num;
	k = digit;
	k2 = digit;
	newNumberSmallest = '';

	if( num == '0' || num == null ){
		alert('Digit a number');
		return;
	}

	if( num.length > 10001 ){
		alert('Length num bigger than 10002');
		return;
	}

	if( num.length == digit ){
		alert('num.length == digit');
		return;
	}

	recursiveRemoveDigits(0, 0);

}


function recursiveRemoveDigits(startIndice, counterRecursive){

	// receive 'startIndice = 0' why not pass in statement 'for';
	var minimunIndice = startIndice;
	var currentMinimunIndice = n[startIndice];

	// I could let 'var i = 0', but will more lazy.
	for(var i = startIndice; i <= startIndice + k; i++){
		var index = n[i];
		if( index <= currentMinimunIndice ){
			// receive less indice found
			minimunIndice = i;
		}
	}

	// add new element
	newNumberSmallest += n[minimunIndice];

	// Without this, loop infinite
	if( counterRecursive == ((n.length - 1) - k2) ){
		//console.log(newNumberSmallest);
		var r = removeLeadingZero(newNumberSmallest);
		//return newNumberSmallest;
		alert(r);
		return;
	}

	// increse to stop recursive
	counterRecursive += 1;

	// I use 'startIndice' in statement 'for' and sum 'k' + 'startIndice'
	// so in statement 'for' we have for example: i = 6; <= 6
	k = k - (minimunIndice - startIndice);
	
	recursiveRemoveDigits((minimunIndice + 1), counterRecursive);
}

function removeLeadingZero(v){
	var replace = function(r){
		if( r[0] == '0' ){
			r = r.substr(1);
			replace(r);
		}
		return r;
	};
	return replace(v);
};