'use strict';

// class Vehicle
function Vehicle(size, type){
	// We will consider size as Spot 
	this.size = size;
	this.type = type;
};
Vehicle.prototype.getSize = function(){return this.size};
Vehicle.prototype.getType = function(){return this.type};

// class Car
function Car(){};
// inherit
Car.prototype = new Vehicle(1, 'Car');

// class Motocycle
function Motocycle(){};
// inherit
Motocycle.prototype = new Vehicle(1, 'Motocycle');

// class Bus
function Bus(){};
// inherit
Bus.prototype = new Vehicle(5, 'Bus');