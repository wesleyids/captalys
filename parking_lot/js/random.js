'use strict';

// class Random
function Random(){};
Random.prototype = new Algorithm();

Random.prototype.find = function(){
	var spot = getRandomSpot();
	var object = {};

	if( spot <= list.length ){
		var item = list[spot],
			isOccupied = '';

		if( item.isOccupied == true ){
			isOccupied = 'Occupied';
		}else if( item.isOccupied == false ){
			isOccupied = 'Free';
		}

		object.msg = 'Found spot on level *'+item.level+'* spot *'+item.id+'*, this spot is: '+isOccupied;
	}

	return object;
};

Random.prototype.insert = function(level, id, vehicle){
	
	var random = new Random(),
		spot = getRandomSpot(),
		item = list[spot];

	var object = {msg: ''};

	if( spot < list.length && TOTALSPOTSOCCUPIED < list.length ){

		if( item && item.isOccupied == true ){
			
			// chamada recursiva
			random.insert(level, id, vehicle);

		}else if( item && item.isOccupied == false ){
			
			object = {
				msg: 'Add new spot on level *'+item.level+'* spot *'+item.id+'*',
				id: item.id,
				level: item.level,
				vehicle: vehicle,
				isOccupied: true
			};
			
			// insert
			list[spot].isOccupied = true;
			list[spot].vehicle = vehicle;
			list[spot].createdAt = new Date();
			
			TOTALSPOTSOCCUPIED += 1;

			return object;
		}
	}else{
		return object.msg = 'Nothing spot empty';
	}

	return object;
};

Random.prototype.remove = function(level, id){
	var random = new Random(),
		spot = getRandomSpot(),
		item = list[spot];

	var object = {};

	if( spot <= list.length ){

		if( item && item.isOccupied == false ){
			
			// chamada recursiva
			// Maximum call stack size exceeded
			random.remove(level, id);

		}else if( item && item.isOccupied == true ){

			object = {
				msg: 'Remove spot on level *'+item.level+'* spot *'+item.id+'*',
				id: item.id,
				level: item.level,
				vehicle: '',
				isOccupied: false
			};
			
			// remove
			list[spot].isOccupied = false;
			list[spot].vehicle = '';
			list[spot].removedAt = new Date();
			
			TOTALSPOTSOCCUPIED -= 1;
		}else{
			// chamada recursiva
			// dificilmente passará aqui, mas vamos garantir a execução.
			random.remove(level, id);
		}

	}

	return object;
};

// Retorna um numero qualquer menor que 'list.length'
function getRandomSpot(){
	return Math.floor((Math.random() * list.length));
};