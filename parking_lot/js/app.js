'use strict';

// global
var LEVEL = 0;
var SPOT  = 0;
var TYPEOFALGORITHM = '';

// Essa variavel serve para controlar a quantidade de spots ocupados.
// Se a quantidade de spots for igual a 'list.length' então está cheio.
var TOTALSPOTSOCCUPIED = 0;

function start(){
	var level = view.getElement('#level'),
		spot  = view.getElement('#spot'),
		typeVehicle = view.getElement('#typeVehicle'),
		operation = view.getElement('#operation'),
		typeOfAlgorithm = view.getElement('#typeOfAlgorithm'),
		levelIdValue = view.getElement('#levelId').value,
		data = {},
		split = function(v){
			v = v.split(';');
			return {level: v[0], id: v[1] };
		},
		find = function(data){
			var r = null;
			for( var i = 0; i < data.childElementCount; i++ ){
				var item = data.children[i];
				if( item && item.checked && item.value ){
					r = item.value;
					break;
				}
			}
			return r;
		};

	if( view.initConfig == false ){
		view.config();
	}

	typeVehicle = find(typeVehicle);
	if( typeVehicle == null ){
		alert('Select vehicle');
		return;
	}

	operation = find(operation);
	if( operation == null ){
		alert('Select operation');
		return
	}

	typeOfAlgorithm = find(typeOfAlgorithm);
	if( typeOfAlgorithm == null ){
		alert('Select algorithm');
		return;
	}

	if( levelIdValue ){
		data = split(levelIdValue);
	}

	new Controller(typeVehicle, typeOfAlgorithm, operation, data);
};


// population
var list = [];
var listIndice = [];
function dataList(level, spot){
	list.push({
		id: spot,
		level: level,
		isOccupied: false
	});
};
function dataListIndice(level, spot){
 	// has other ways to make this
 	if( !listIndice[level] ){
 		listIndice[level] = [];
 	}
 	listIndice[level].push({
 		id: spot,
 		isOccupied: false
 	});
};


// ignore
var view = {
	initConfig: false,
	getElement: function(el){
		return document.querySelector(el);
	},
	createElement: function(tag){
		return document.createElement(tag);
	},
	appendElement: function(target, el){
		//document.body.append(el);
		target.append(el);
	},
	_alert: function(v, t){
		if( t == 'console' ){
			console.log(v);
		}else{
			alert(v);
		}
	},
	// add only color in this function
	addSpot: function(id, bg, data){
		var a = view.getElement(id);
		a.style.background = bg;
		
		if( data && data.vehicle != undefined ){
			data.vehicle = ', vehicle: '+data.vehicle;
		}else{
			data.vehicle = '';
		}

		a.innerHTML = '<span>{id: '+data.id+ data.vehicle +'}</span>';
	},
	totalSpots: function(t, l){
		if( t != 'Indice' ){
			l = list.length;
		}
		//view.getElement('#totalSpots').innerText = 'Total spots: ' + l;
	},
	spotsFree: function(v){
		//view.getElement('#freeSpots').innerText = 'Free spots: ' + v;
	},
	spotOccupieds: function(v){
		//view.getElement('#occupiedSpots').innerText = 'Occupied spots: ' + v;
	},
	config: function(algoTest){
		var levels = view.getElement('#level').value,
			spots  = view.getElement('#spot').value,
			levelsArr = [],
			typeOfAlgorithm = view.getElement('#typeOfAlgorithm'),
			parkingLot = view.getElement('#parkingLot');

		list = [];
		listIndice = [];
		parkingLot.innerHTML = '';
		
		// uso elas no teste.js
		LEVEL = levels;
		SPOT  = spots;

		view.initConfig = true;

		if( levels == 0 || spot == 0 ){
			throw Error('Level/Spot equals zero');
		}

		if( algoTest == undefined ){
			for( var i = 0; i < typeOfAlgorithm.childElementCount; i++ ){
				if( typeOfAlgorithm.children[i] &&
					typeOfAlgorithm.children[i].checked &&
					typeOfAlgorithm.children[i].value == 'Indice'
				){
					typeOfAlgorithm = 'Indice';
					TYPEOFALGORITHM = 'Indice';
					break;
				}
			}
		}else{
			typeOfAlgorithm = algoTest;
		}

		for( var i = 0; i < levels; i++ ){
			levelsArr[i] = new Array(spots / levels);
		}

		// Rendering
		// Can be slow here above 10.000 elements. We can use Worker() not to brake.
		for( var i = 0; i < levelsArr.length; i++ ){
			var level = view.createElement('div'),
				inc = i + 1;
			
			level.id  = 'level_'+i;
			level.innerHTML = '<div>Level '+ i +'</div>';
			level.classList.add('level');

			for( var y = 0; y < levelsArr[i].length; y++ ){
				var spot = view.createElement('div');
				spot.id = 'level_'+ i + '_spot_' + y;
				spot.classList.add('spot');
				//spot.classList.add('spot-free');
				spot.innerHTML = '<span>id: ' + y + '</span>';
				level.append(spot);

				// create data list for simulation
				if( typeOfAlgorithm == 'Indice' ){
					dataListIndice(i, y);
				}else{
					dataList(i, y);
				}
			}

			if( (spots / levels) == 10 ){
				view.appendElement(parkingLot, level);
			}
		}

		// total spots
		//view.totalSpots(typeOfAlgorithm, levels);
	}
};