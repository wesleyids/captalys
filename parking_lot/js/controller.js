'use strict';

// class controller
var Controller = function(typeVehicle, typeOfAlgorithm, operation, data){

	var vehicle = null,
		algorithm = null;

	if( typeVehicle == 'Car' ){
		vehicle = new Car();
	}else if( typeVehicle == 'Bus' ){
		vehicle = new Bus();
	}else if( typeVehicle == 'Motocycle' ){
		vehicle = new Motocycle();
	}

	// passar para o algoritmo correto
	if( typeOfAlgorithm == 'Linear' ){
		algorithm = new Linear();
	}else if( typeOfAlgorithm == 'Random' ){
		algorithm = new Random();
	}else if( typeOfAlgorithm == 'Indice' ){
		algorithm = new Indice();
	}

	var retorno = algorithm.run(algorithm, operation, vehicle, data);
	new View(retorno);

};