'use strict';

// class Linear
function Linear(){};
Linear.prototype = new Algorithm();

Linear.prototype.find = function(level, id){

	var object = {};

	if( level == undefined && id == undefined ){
		object.msg = 'Insert *level* and *spot* to find spot';
		return object;
	}

	for( var i = 0; i < list.length; i++ ){
		var item = list[i],
			found = false;
		if( item.level == level && item.id == id ){
			var isOccupied = '';

			if( item.isOccupied == true ){
				isOccupied = 'Occupied';
			}else if( item.isOccupied == false ){
				isOccupied = 'Free';
			}

			object.msg = 'Found spot on level *'+item.level+'* spot *'+item.id+'*, this spot is: '+isOccupied;
			found = true;
			break;
		}

		if( i == (list.length - 1) && found == false ){
			object.msg = 'Not found spot';
		}
	}

	return object;
};

Linear.prototype.insert = function(level, id, vehicle){

	var object = {};

	for( var i = 0; i < list.length; i++ ){
		var isOccupied = list[i].isOccupied,
			item = list[i];

		object = {
			msg: '',
			id: item.id,
			level: item.level,
			vehicle: vehicle
		};

		if( level != undefined && id != undefined ){
			if( item.level == level && item.id == id ){
				if( isOccupied == true ){
					object.msg = 'This level *'+level+'* spot *'+id+'* is occupied';
					break;
				}else{

					object.msg = 'Add new spot on level *'+level+'* spot *'+id+'*';
					object.id = id;
					object.level = level;
					object.isOccupied = true;
					
					// insert
					list[i].isOccupied = true;
					list[i].vehicle = vehicle;
					list[i].createdAt = new Date();
					
					break;
				}
			}
		}else if( level == undefined && id == undefined && isOccupied == false ){

			object.msg = 'Add new spot on level *'+item.level+'* spot *'+item.id+'*';
			object.isOccupied = true;

			// insert
			list[i].isOccupied = true;
			list[i].vehicle = vehicle;
			list[i].createdAt = new Date();
			
			break;
		}
	}

	return object;
};

Linear.prototype.remove = function(level, id){

	var object = {};

	if( level == undefined && id == undefined ){
		object.msg = 'Insert *level* and *spot* to remove spot';
		return object;
	}

	for( var i = 0; i < list.length; i++ ){
		var isOccupied = list[i].isOccupied,
			item = list[i],
			found = false;

		object = {
			msg: '',
			id: item.id,
			level: item.level,
			vehicle: ''
		};

		if( item.level == level && item.id == id ){
			if( isOccupied == true ){

				object.msg = 'Remove spot on level *'+level+'* spot *'+id+'*';
				object.id = id;
				object.level = level;
				object.isOccupied = false;
				
				list[i].isOccupied = false;
				list[i].vehicle = '';
				list[i].removedAt = new Date();
				
				found = true;
				break;
			}else{
				object.msg = 'Spot on level *'+level+'* spot *'+id+'* not occupied';
			}
		}

		if( i == (list.length - 1) && found == false ){
			object.msg = 'Nothing spot removed';
		}
	}

	return object;
};