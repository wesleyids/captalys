'use strict';

// class View
var View = function(data){

	var _id = '',
		color = '',
		isValid = false,
		_data = {};

	if( data && data.id >= 0 && data.level >= 0 ){
		_id  = '#level_'+data.level+'_spot_'+data.id, //level_0_spot_0
		_data.id = data.id;
		_data.vehicle = data.vehicle;
		isValid = true;
	}

	if( data && (data.isOccupied == true || data.isOccupied == false) ){
		if( data.isOccupied == true ){
			color = '#e88787';
			isValid = true;
		}else if( data.isOccupied == false ){
			color = '#80e080';
			isValid = true;
		}
	}else{
		isValid = false;
	}

	if( isValid ){
		view.addSpot(_id, color, _data);
		view._alert(data.msg, 'console');
	}else{
		view._alert(data.msg, 'console');
	}
};