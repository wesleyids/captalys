'use strict';

function Indice(){};
Indice.prototype = new Algorithm();

Indice.prototype.find = function(level, id){

	var object = {};

	for( var i = 0; i < listIndice.length; i++ ){

		var item = listIndice[i];

		// Aqui considerei o 'i' como level
		if( i == level ){
			for( var y = 0; y < item.length; y++ ){

				var subItem = item[y];

				if( subItem.id == id ){
					var isOccupied = '';

					if( subItem.isOccupied == true ){
						isOccupied = 'Occupied';
					}else if( subItem.isOccupied == false ){
						isOccupied = 'Free';
					}

					object.msg = 'Found spot on level *'+i+'* spot *'+subItem.id+'*, this spot is: '+isOccupied;
					break;
				}
			}
			break;
		}
	}

	return object;
};

Indice.prototype.insert = function(level, id, vehicle){
	
	var object = {}

	if( level == undefined && id == undefined ){
		view._alert('Insert *level* and *spot* to insert spot');
		return;
	}

	for( var i = 0; i < listIndice.length; i++ ){

		var item = listIndice[i];

		// Aqui considerei o 'i' como level
		if( i == level ){
			for( var y = 0; y < item.length; y++ ){
				
				var subItem = item[y];

				if( subItem.id == id ){
					if( subItem.isOccupied == false ){

						object = {
							msg: 'Add new spot on level *'+i+'* spot *'+subItem.id+'*',
							id: subItem.id,
							level: i,
							vehicle: vehicle,
							isOccupied: true
						};

						// insert
						listIndice[i][y].isOccupied = true;
						listIndice[i][y].vehicle = vehicle;
						listIndice[i][y].createdAt = new Date();
					}else{
						object.msg = 'This spot on level *'+i+'* spot *'+subItem.id+'* is occupied';
					}
					break;
				}
			}
			break;
		}
	}

	return object;
};

Indice.prototype.remove = function(level, id){
	
	var object = {};

	if( level == undefined && id == undefined ){
		view._alert('Insert *level* and *spot* to remove spot');
		return;
	}

	for( var i = 0; i < listIndice.length; i++ ){

		var item = listIndice[i];

		// Aqui considerei o 'i' como level
		if( i == level ){

			for( var y = 0; y < item.length; y++ ){
				var subItem = item[y];

				if( subItem.id == id ){
					if( subItem.isOccupied == true ){
						object = {
							msg: 'Remove spot on level *'+i+'* spot *'+subItem.id+'*',
							id: subItem.id,
							level: i,
							vehicle: '',
							isOccupied: false
						};

						listIndice[i][y].isOccupied = false;
						listIndice[i][y].vehicle = '';
						listIndice[i][y].removedAt = new Date();

					}else{
						object.msg = 'This spot on level *'+i+'* spot *'+subItem.id+'* is free';
					}
					break;
				}
			}
		}
	}

	return object;
};