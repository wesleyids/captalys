'use strict';

// class Algorithm
function Algorithm(){};

Algorithm.prototype.find = function(level, id){};
Algorithm.prototype.insert = function(level, id, vehicle){};
Algorithm.prototype.remove = function(level, id){};

Algorithm.prototype.run = function(algorithm, op, vehicle, data){

	if( op == 'search' ){
		return algorithm.find(data.level, data.id);
	}else if( op == 'insert' ){
		return algorithm.insert(data.level, data.id, vehicle.getType());
	}else if( op == 'remove' ){
		return algorithm.remove(data.level, data.id);
	}

};