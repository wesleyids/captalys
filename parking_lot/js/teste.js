'use strict';

var t = {
	ALGORITHM: [ 'Linear', 'Random', 'Indice' ],
	VEHICLES : [ 'Car', 'Bus', 'Motocycle' ],
	OPERATION: [ 'insert', 'remove', 'search' ],

	// Default 30
	// Change value for increase or decrease time of operation of test.
	stop: 30,

	getRandom: function(v){
		return Math.floor((Math.random() * v));
	},

	init: function(){

		var algorithm = t.ALGORITHM[ t.getRandom(t.ALGORITHM.length) ];
		var start = 0;
		var stop = t.start; // vai terminar em 30 seg
		var p = {};

		//algorithm = 'Random';

		view.config(algorithm);
		view._alert('Algorithm: '+algorithm, 'console');

		if( (SPOT / LEVEL ) == 10 ){
			p = { level: LEVEL, spot: (SPOT / LEVEL ) };
		}

		setInterval(function(){
			if( start < stop ){
				start += 1;

				var operation = t.OPERATION[ t.getRandom(t.OPERATION.length) ];
				var vehicle   = t.VEHICLES[ t.getRandom(t.VEHICLES.length) ];
				var data = {};
				var spot = undefined;
				var level = undefined;

				spot  = t.getRandom(p.spot);
				level = t.getRandom(p.level);
				data = {level: level, id: spot};

				if( algorithm == 'Random' ){
					data = {};
				}

				if( algorithm == 'Linear' ){
					if( operation == 'insert' ){
						data = {};
					}
				}

				view._alert('Operation: '+operation, 'console');

				Controller(vehicle, algorithm, operation, data);

			}
		}, 1000);

	}
};